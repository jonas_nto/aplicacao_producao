﻿using Robo_ViaVarejo.Models.Aplicacao_TP;
using Robo_ViaVarejo.Views.WEB;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Robo_ViaVarejo.Controller
{
    class Controlador : Functions
    {
        #region Variaveis Públicas
        public static int row_start = 3;
        public static int row_count;
        public static int Plan_Exec;
        public static string fluxo_Robo;
        public static string ambiente_;
        public static string facility_c;
        public static string filial_c;
        public static string init = null;
        public static int count_prc;
        public static string Fil_Status;
        public static string Sheet;
        public static string barcode_wms;
        public static string EAN;
        public static string User_WMS_ARM_Prod = "7700329460";
        public static string Senha_User_WMS_ARM_Prod = "password123";
        public static string User_WMS_ARM_PreProd = "wmadmin";
        public static string Senha_User_WMS_ARM_PreProd = "password";

        #endregion

        public static void Controller(string facility, string filial, string fluxoRobo, string ambiente)
        {

            Plan_Exec = 0;
            facility_c = facility;
            filial_c = filial;
            fluxo_Robo = fluxoRobo;
            ambiente_ = ambiente;

            Verify_Process_Excel();
            xlApp.Visible = true;

            Fluxo();

            #region Excel(Save|Close|Chrome|mensagem de finalizacao)
            //Save Excel
            xlWorkbook.Save();
            xlWorkbook.Close();
            Kill_Process("chrome");
            Console.WriteLine("Robo Finalizado!");
            //Kill_Process("pcsws");
            #endregion
        }

        public static void drawTextProgressBar(string stepDescription, int progress, int total)
        {
            int totalChunks = 30;

            //draw empty progress bar
            Console.CursorLeft = 0;
            Console.Write("["); //start
            Console.CursorLeft = totalChunks + 1;
            Console.Write("]"); //end
            Console.CursorLeft = 1;

            double pctComplete = Convert.ToDouble(progress) / total;
            int numChunksComplete = Convert.ToInt16(totalChunks * pctComplete);

            //draw completed chunks
            Console.BackgroundColor = ConsoleColor.Green;
            Console.Write("".PadRight(numChunksComplete));

            //draw incomplete chunks
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.Write("".PadRight(totalChunks - numChunksComplete));

            //draw totals
            Console.CursorLeft = totalChunks + 5;
            Console.BackgroundColor = ConsoleColor.Black;

            string output = progress.ToString() + " of " + total.ToString();
            Console.Write(output.PadRight(15) + stepDescription); //pad the output so when changing from 3 to 4 digits we avoid text shifting
        }

        public static void Controle_execucao()
        {
            string Status = Read_Record(Option.Read, "Sheet", "Status", null).Valor;
            if (Status.Equals("Executando"))
            {
                if ((Read_Record(Option.Read, "Sheet", "Inicio", null).Valor) != null)
                {
                    Read_Record(Option.Record, "Sheet", "Fim", DateTime.Now.ToString("hh:mm:ss"));
                }
            }
            else if (Status.Equals("Executar"))
            {
                Read_Record(Option.Record, "Sheet", "Inicio", DateTime.Now.ToString("hh:mm:ss"));
            }
        }

        public static bool Fluxo()
        {
            try
            {
                if (row_start == 3)

                switch (facility_c.ToUpper())
                {
                    case "ITEMS":
                        switch (fluxo_Robo)
                        {
                            case "Mudança no codigo EAN de item":
                                    if (!Views.WEB.Armazem.Config_Itens.Alteracao_Multiplos_EANs()) return false;
                                break;
                        }
                        break;

                    case "DAT":
                        break;
                }

 
                return true;
            }
            catch (Exception)
            {
                Fluxo();
            }
            return false;
        }

        private static Boolean Verify_Process_Excel()
        {
            bool function = true;

            try
            {
                if (Plan_Exec == 0)
                {
                    //Kill Processes
                    Kill_Process("Excel");

                    Plan_Exec++;

                    //Open Excel
                    Open_Excel(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Aplicacao_Producao", "Gerenciador.xlsx"));

                    for (int x = 0; x <= 6; x++)
                    {
                        var process = Process.GetProcessesByName("Excel");

                        foreach (var p in process)
                        {
                            if (p.MainWindowTitle.Contains("Excel"))
                            {
                                Worksheet = xlWorkbook.Sheets["Gerenciador"];
                                XlRange = Worksheet.UsedRange;
                                row_count = XlRange.Rows.Count;
                                return function;
                            }
                        }
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    Plan_Exec++;

                    for (int x = 0; x <= 50; x++)
                    {
                        var process = Process.GetProcessesByName("Excel");

                        foreach (var p in process)
                        {
                            if (p.MainWindowTitle.Contains("Excel"))
                            {
                                Worksheet = xlWorkbook.Sheets["Gerenciador"];
                                XlRange = Worksheet.UsedRange;
                                row_count = XlRange.Rows.Count;
                                return function;
                            }
                        }
                        Thread.Sleep(500);
                    }
                    //Open Excel
                    Open_Excel(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\ArenaEstacao", "ArenaEstacao.xlsm"));
                    Thread.Sleep(3000);
                    Verify_Process_Excel();
                }
                return function;
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
                return function = false;
            }
        }
    }
}