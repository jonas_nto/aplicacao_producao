﻿using AutoIt;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Robo_ViaVarejo.Controller;
using Robo_ViaVarejo.Models.Aplicacao_TP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

public class Driver
{
    public static ChromeDriver driver = null;
    public static ChromeOptions options;

    public static ChromeDriver SetUp(string link)
    {
        ChromeOptions options = new ChromeOptions();
        #region Options_Driver
        options.AddArguments("test-type");
        options.AddArgument("--start-maximized");
        options.AddArguments("--disable-web-security");
        options.AddArguments("--allow-running-insecure-content");
        options.AddArguments("--ignore-certificate-errors");
        options.AddArguments("--ignore-urlfetcher-cert-requests");
        //options.AddExtension(@"C:\Users\7700330507\source\repos\Robo_ViaVarejo\BlazeMeter-_-The-Continuous-Testing-Platform_v4.6.0.crx");
        #endregion

        driver = new ChromeDriver(options);

        //Abertura da Aplicação
        try
        {
            OpenUrl(link, driver);
            return driver;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
    }//End public static void SetUp

    public static bool OpenUrl(string address, ChromeDriver driver)
    {
        for (int i = 0; i < 10; i++)
            try
            {
                driver.Navigate().GoToUrl(address);
                return true;
            }
            catch (Exception)
            {
                driver = new ChromeDriver(options);
                Thread.Sleep(1000);
            }
        return false;
    }

    //______________________________________________________________

    public static ChromeDriver SetUp_Send(string link, bool user = true)
    {
        ChromeOptions options = new ChromeOptions();
        #region Options_Driver
        options.AddArguments("test-type");
        options.AddArgument("--start-maximized");
        options.AddArguments("--disable-web-security");
        options.AddArguments("--allow-running-insecure-content");
        options.AddArguments("--ignore-certificate-errors");
        options.AddArguments("--ignore-urlfetcher-cert-requests");
        #endregion

        driver = new ChromeDriver(options);

        //Abertura da Aplicação
        try
        {            
            OpenUrl_Send(link, driver, user);
            return driver;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
    }//End public static void SetUp

    public static bool OpenUrl_Send(string address, ChromeDriver driver, bool user = true)
    {
        for (int i = 0; i < 10; i++)
            try
            {
                Thread.Sleep(1000);
                AutoItX.Send("{F6}");
                Thread.Sleep(500);
                AutoItX.Send(address);
                AutoItX.Send("{ENTER}");
                Thread.Sleep(1000);
                if (user == true)
                {
                    AutoItX.Send("Administrator");
                    AutoItX.Send("{TAB}");
                    AutoItX.Send("manage");
                    AutoItX.Send("{ENTER}");
                }
                return true;
            }
            catch (Exception)
            {
                driver = new ChromeDriver(options);
                Thread.Sleep(1000);
            }
        return false;
    }



}


