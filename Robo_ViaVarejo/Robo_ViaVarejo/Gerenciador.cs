﻿using Robo_ViaVarejo.Controller;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows.Forms;

namespace Robo_ViaVarejo
{
    public partial class Gerenciador : Form
    {


        public Gerenciador()
        {
            InitializeComponent();

            Facility_Select.Items.Add("Items");

            listAmbiente.Items.Add("Producao");
            listAmbiente.Items.Add("PreProducao");
            play.Visible = false;

            Filial_Select.SelectedItem = "";
            Operacao_Robo.SelectedItem = "";
            listAmbiente.SelectedItem = "";  

        }

        private void listFluxo_Robo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Facility_Select.SelectedItem.ToString() != "" && Filial_Select.SelectedItem.ToString() != "" && Operacao_Robo.SelectedItem.ToString() != "" && listAmbiente.SelectedItem.ToString() != "")
                    play.Visible = true;
                else
                    play.Visible = false;

                string link = "";
                SystemStatus.Text = "";

                if (Operacao_Robo.SelectedItem.ToString() != "")
                {
                    switch (Operacao_Robo.SelectedItem.ToString())
                    {
                        case "Roteirização WEB":
                            // Monta a requisição Http
                            link = "https://dsweb01/pista07/gateway?hptAppId=W1A1&hptExec=Y";
                            HttpWebRequest Link = (HttpWebRequest)HttpWebRequest.Create(link);
                            // Faz a requisição e obtém o retorno
                            HttpWebResponse Link_ = (HttpWebResponse)Link.GetResponse();

                            if (Link_.StatusCode == HttpStatusCode.OK)
                            {
                                SystemStatus.Text = "Link: " + link + " | Status: Online";
                            }
                            else
                            {
                                SystemStatus.Text = "Link: " + link + " | Status: Offline";
                            }
                            break;
                        case "Pedido On-Line":
                            // Monta a requisição Http
                            link = "http://carrinho.pontofriointegracaohlg.net/";
                            HttpWebRequest Roteirizacao = (HttpWebRequest)HttpWebRequest.Create(link);
                            // Faz a requisição e obtém o retorno
                            HttpWebResponse Roteirizacao_ = (HttpWebResponse)Roteirizacao.GetResponse();


                            if (Roteirizacao_.StatusCode == HttpStatusCode.OK)
                            {
                                SystemStatus.Text = "Status do Servidor: Online";
                            }
                            else
                            {
                                SystemStatus.Text = "Status do Servidor: Offline";
                            }
                            break;
                        default:
                            SystemStatus.Text = "";
                            break;
                    }
                }
            }
            catch
            { SystemStatus.Text = "Status do Servidor: Offline"; }

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            string ambiente = listAmbiente.SelectedItem.ToString();
            string operacao = Operacao_Robo.SelectedItem.ToString();
            string facility = Facility_Select.SelectedItem.ToString();
            string filial = Filial_Select.SelectedItem.ToString();

            Console.WriteLine("****  Executando o Robô  *****");
            Console.WriteLine("Ambiente: " + ambiente);
            Console.WriteLine("Facility: " + facility);
            Console.WriteLine("Filial: " + filial);
            Console.WriteLine("Operação: " + operacao);

             Controlador.Controller(facility, filial, operacao, ambiente);
        }

        private void Gerenciador_Load(object sender, EventArgs e)
        {            
        }

        private void Fechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void listAmbiente_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void label6_Click(object sender, EventArgs e)
        {
        }

        private void Filial_Select_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void Facility_Select_SelectedIndexChanged(object sender, EventArgs e)
        {
            string link = "";

            switch (Facility_Select.SelectedItem.ToString())
            {
                case "Items":
                    Operacao_Robo.Items.Clear();
                    Operacao_Robo.Text = "";
                    Operacao_Robo.Items.Add("Mudança no codigo EAN de item");
                    Filial_Select.Items.Add("1475");//pinhais
                    Filial_Select.Items.Add("2241");//marituba
                    Filial_Select.Items.Add("1522");//DAT_Pinhais
                    break;
                default:
                    Operacao_Robo.Text = "";
                    Operacao_Robo.Items.Clear();
                    break;
            }
            
        }
    }
}
