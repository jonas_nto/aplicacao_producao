﻿
using OpenQA.Selenium;

namespace Robo_ViaVarejo.Views.WEB
{
    public static class PageObjectsWEB
    {
        #region Manhatthan

        #region Logins Ambientes
        #region Tela 1 - Login |          new - "https://alm1200wmosprod.wms.viavarejo.com.br:12001"    | old -  http://mht-dev-app02.dc.nova:22000/manh/index.html
        public static string url_DAT_Almoxarifado = "https://mhtg8wmospprdn1.dc.nova:22001";
        public static string url_Armazem_Contagem_PreProd = "https://mhtg7wmospreprodn1.dc.nova:12001/manh/index.html?i=102";
        public static string url_DAT_Pinhais_PreProd = "https://mhtg8wmospprdn1.dc.nova:22001/manh/index.html?i=123";
        public static string url_DAT_Contagem = "https://mhtg7wmospreprodn1.dc.nova:22001/manh/index.html?i=102";
        public static string url_Armazem_Pinhais_PreProd = "https://mhtg8wmospprdn1.dc.nova:12001/manh/index.html";
        public static string url_Armazem_Marituba_PreProd = "https://mhtg3wmospreprodn1.dc.nova:42001/";
        public static string url_Armazem_Pinhais_Prod = "https://arm1475wmosprod.wms.viavarejo.com.br:12001/manh/index.html?i=122";
        public static string url_Armazem_Marituba_Prod = "https://arm2241wmosprod.wms.viavarejo.com.br:42001";
        public static string url_DAT_Pinhais_Prod = "https://dat1522wmosprod.wms.viavarejo.com.br:22001/manh/index.html?i=103";

        public static By login_Usuario_app = By.Id("username");
        public static By senha_Usuario_app = By.Id("password");
        public static By entra_Login_app = By.Id("loginButton");
        #endregion

        #region Tela 1 - Login Pedido Online |  http://carrinho.pontofriointegracaohlg.net/?idSku=
        public static string url_Pedido_Online = "http://carrinho.pontofriointegracaohlg.net/?idSku=";
        public static By login_Usuario_po = By.ClassName("iEmail");
        public static By senha_Usuario_po = By.ClassName("iPassword");
        public static By processa_po = By.Id("btnClienteLogin");
        public static By entra_Login_po = By.Id("btSelecionarPagamento");
        #endregion

        #region Tela 1 - Login Roteirização | https://dsweb01/pista07/gateway?hptAppId=W1A1&hptExec=Y
        public static string url_Roteirizacao = "https://dsweb01/pista07/gateway?hptAppId=W1A1&hptExec=Y";
        public static By login_Empresa_rot = By.Name("CD_EMPGCB_FUN");
        public static By Usuario_rot = By.Name("CD_FUN");
        public static By senha_Usuario_rot = By.Name("CD_USRSGR_SNH_CPL");
        public static By processa_Login_rot = By.Name("NM_BOT_PRC");
        #endregion

        #region Tela 2 - Aplicação 

        #endregion
        #endregion
        #endregion
    }
}
