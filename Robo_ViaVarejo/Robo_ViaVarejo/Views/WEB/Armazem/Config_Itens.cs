﻿using System;
using Robo_ViaVarejo.Controller;
using Robo_ViaVarejo.Models.WEB;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;

namespace Robo_ViaVarejo.Views.WEB.Armazem
{
    class Config_Itens : Controlador
    {
        public static string Sku_ant = "S/R";//Sem registro
        public static List<string> Skus;
        public static List<string> EANs;
        public static List<string> EANs_exists;
        public static string iniciado;
        public static int count = 0;

        public static bool Alteracao_Multiplos_EANs()
        {
            try
            {
                Worksheet = xlWorkbook.Sheets["Alteração Multiplos EANs"];
                XlRange = Worksheet.UsedRange;
                row_count = XlRange.Rows.Count;
                Sheet = "Alteração Multiplos EANs";

                for (int row = row_start; row <= row_count; row++)
                {
                    row_start = row;
                    barcode_wms =  Read_Record(Option.Read, Sheet, "Barcode WM", null).Valor;
                    EAN = Read_Record(Option.Read, Sheet, "Business Partner Item Barcode", null).Valor;
                    switch (filial_c) 
                    { case "1475": Fil_Status = "Status_1475"; break; 
                     case "1522": Fil_Status = "Status_1522"; break; 
                     case "2241": Fil_Status = "Status_2241"; break; }
                    String Status = Read_Record(Option.Read, Sheet, Fil_Status, null).Valor;

                    string status;
                    if (row == row_count - 1)
                        status = "Executando";
                    else
                        status = "";
                    drawTextProgressBar(status, row, row_count);
                    if (Status == null)
                    {
                        if (Login.Login_WMS() == true)
                        {
                            if (Sku_ant == "S/R")
                            {
                                Skus = new List<string>();
                                EANs = new List<string>();
                                Skus.Add(barcode_wms);
                                EANs.Add(EAN);
                                Sku_ant = barcode_wms;
                            }
                            else if (Sku_ant == barcode_wms)
                            {
                                Skus.Add(barcode_wms);
                                EANs.Add(EAN);
                                Sku_ant = barcode_wms;
                            }
                            else if (Abre_menu_Config())
                            {
                                if (Insere_multiplos_EANs())
                                {
                                    count_prc++; row = row - 1;
                                    if (count_prc > 400)
                                    {
                                        Functions_WEB.Fecha_menu(1); iniciado = null; init = null;
                                        xlWorkbook.Save();
                                        Kill_Process("chrome");
                                        Console.WriteLine("Reiniciando Robô!");
                                        count_prc = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static bool Abre_menu_Config()
        {
            try
            {
                if (iniciado == null)
                {
                    //entrar no menu Items (configuration)
                    if (!Functions_WEB.Menu_Tipo("//*[@id='button-1013']", "x-hbox-form-item", "items", "Items (Configuration)")) return false;
                    Functions_WEB.MudaFrameTela(1);
                    iniciado = "Sim";
                }
                Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemList_lv:ItemList_filterId:itemLookUpId"), Sku_ant);
                Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemList_lv:ItemList_filterId:ItemList_filterIdapply"), null);
                if (Functions_WEB.VerificaObjetoExistente(By.Id("checkAll_c0_dataForm:ItemList_lv:dataTable"), true))
                {
                    string valida_item = driver.FindElement(By.Id("dataForm:ItemList_lv:dataTable:0:itemList_Link_NameText")).Text;

                    while (valida_item != Sku_ant)
                    {
                        driver.FindElement(By.Id("dataForm:ItemList_lv:ItemList_filterId:itemLookUpId")).Clear();
                        Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemList_lv:ItemList_filterId:itemLookUpId"), Sku_ant);
                        Thread.Sleep(1000);
                        Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemList_lv:ItemList_filterId:ItemList_filterIdapply"), null);
                        valida_item = driver.FindElement(By.Id("dataForm:ItemList_lv:dataTable:0:itemList_Link_NameText")).Text;
                    }
                    Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("checkAll_c0_dataForm:ItemList_lv:dataTable"), null);
                    Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemList_Viewbutton"), null);
                }
                else
                {
                    for (int i = 0; i < Skus.Count; i++)
                    {
                        row_start = row_start - 1;
                        Read_Record(Option.Record, Sheet, Fil_Status, "S/C");
                    }
                    Sku_ant = "S/R";
                }
                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

        }

        public static bool Insere_multiplos_EANs()
        {
            try
            {
                Functions_WEB.PreencheValorCampoSetSelectButton(By.LinkText("X-References"), null);
                if (Functions_WEB.VerificaObjetoExistente(By.TagName("h3"), true, null, 5))
                {
                    string validacao = driver.FindElement(By.TagName("h3")).Text;
                    if (validacao == "ERROR")
                    {
                        Functions_WEB.Fecha_menu(1);
                        iniciado = null;
                        Abre_menu_Config();
                    }
                }
                EANs_exists = new List<string>();
                for (int i = 0; i < 30; i++)
                {
                    if (Functions_WEB.VerificaObjetoExistente(By.Id("dataForm:ItemSupplierXRefListEV_dataTable:" + i + ":ItemSupplierXRefListEV_optxt_SupplierItemBarCode"), true, null, 2))
                    {
                        string ean = driver.FindElement(By.Id("dataForm:ItemSupplierXRefListEV_dataTable:" + i + ":ItemSupplierXRefListEV_optxt_SupplierItemBarCode")).Text;
                        EANs_exists.Add(ean);
                    }
                    else { break; }
                }
                Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemDetailsMain_Edit_button"), null);
                if (Functions_WEB.VerificaObjetoExistente(By.Id("dataForm:ItemSupplierXRefListEV_addbutton"), true, null, 40))
                {
                    for (int i = 0; i < Skus.Count; i++)
                    {
                        if (!EANs_exists.Contains(EANs[i]))
                        {
                            Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemSupplierXRefListEV_addbutton"), null);
                            Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemSupplierXRefListEV_dataTable:newRow_" + (count + 1) + ":ItemSupplierXRefListEV_inputMask_SupplierItemBarCode"), EANs[i]);
                            Functions_WEB.PreencheValorCampoSetSelectButton(By.Id("dataForm:ItemSupplierXRefListEV_dataTable:newRow_" + (count + 1) + ":ItemSXRefListEV_SQty_IM"), "1");
                            count++;
                        }
                    }
                    Thread.Sleep(1000);
                    driver.FindElement(By.Id("dataForm:ItemDetailsMain_Save_Detail_button")).Click();
                    if (Functions_WEB.VerificaObjetoExistente(By.ClassName("dialog_inner"), true, null, 10))
                    {
                        for (int i = 0; i < Skus.Count; i++)
                        {
                            row_start = row_start - 1;
                            Read_Record(Option.Record, Sheet, Fil_Status, "ERROR");
                        }
                    }
                    else
                    {
                        for (int i = 0; i < Skus.Count; i++)
                        {
                            row_start = row_start - 1;
                            Read_Record(Option.Record, Sheet, Fil_Status, "OK");
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < Skus.Count; i++)
                    {
                        row_start = row_start - 1;
                        Read_Record(Option.Record, Sheet, Fil_Status, "refazer");
                        Thread.Sleep(10000);
                    }
                }

                driver.FindElement(By.Id("backImage")).Click();
                Sku_ant = "S/R";
                count = 0;

                Functions_WEB.VerificaObjetoExistente(By.Id("dataForm:ItemList_lv:ItemList_filterId:itemLookUpId"), true);//voltar
                driver.FindElement(By.Id("dataForm:ItemList_lv:ItemList_filterId:itemLookUpId")).Clear();//limpar onde digitamos o SKU
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
