﻿using Robo_ViaVarejo.Models.WEB;
using Robo_ViaVarejo.Models.Aplicacao_TP;
using System;
using System.Threading;
using OpenQA.Selenium;

namespace Robo_ViaVarejo.Views.WEB
{
    class Login : Controller.Controlador
    {
        public static string usuario;
        public static string senha;
        public static bool Login_WMS()
        {
            if (init == null)
            {
                if (ambiente_ == "PreProducao")
                {
                    try
                    {
                        usuario = User_WMS_ARM_PreProd;
                        senha = Senha_User_WMS_ARM_PreProd;

                        if (filial_c == "1475") { try { SetUp(PageObjectsWEB.url_Armazem_Pinhais_PreProd); } catch (Exception) { return false; } }
                        else if (filial_c == "2241") { try { SetUp(PageObjectsWEB.url_Armazem_Marituba_PreProd); } catch (Exception) { return false; } }
                        else if (filial_c == "1760") { try { SetUp(PageObjectsWEB.url_Armazem_Contagem_PreProd); } catch (Exception) { return false; } }
                    }
                    catch (Exception e) { Console.WriteLine(e.Message); return false; }
                }
                else
                {
                    try
                    {
                        usuario = User_WMS_ARM_Prod;
                        senha = Senha_User_WMS_ARM_Prod;

                        if (filial_c == "1475") { try { SetUp(PageObjectsWEB.url_Armazem_Pinhais_Prod); } catch (Exception) { return false; } }
                        else if (filial_c == "2241") { try { SetUp(PageObjectsWEB.url_Armazem_Marituba_Prod); } catch (Exception) { return false; } }
                        else if (filial_c == "1522") { try { SetUp(PageObjectsWEB.url_DAT_Pinhais_Prod); } catch (Exception) { return false; } }
                    }
                    catch (Exception e) { Console.WriteLine(e.Message); return false; }
                }
                try
                {
                    //Usuario
                    Functions_WEB.PreencheValorCampoSetSelectButton(PageObjectsWEB.login_Usuario_app, usuario);

                    //Senha
                    Functions_WEB.PreencheValorCampoSetSelectButton(PageObjectsWEB.senha_Usuario_app, senha);

                    //Entra
                    Functions_WEB.PreencheValorCampoSetSelectButton(PageObjectsWEB.entra_Login_app, null);
                    Functions_WEB.VerificaObjetoExistente(By.Id("button-1013"), true);
                    init = "S";
                    return true;
                }
                catch (Exception) { return false; }
            }

            else { return true; }
        }
    }
}
