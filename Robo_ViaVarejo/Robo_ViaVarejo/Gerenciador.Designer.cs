﻿namespace Robo_ViaVarejo
{
    partial class Gerenciador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Gerenciador));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Fechar = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Operacao = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.play = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Operacao_Robo = new System.Windows.Forms.ComboBox();
            this.listAmbiente = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SystemStatus = new System.Windows.Forms.Label();
            this.Facility_Select = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Filial_Select = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.play)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(161)))), ((int)(((byte)(21)))));
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.Fechar);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-1, -3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(812, 29);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Robo_ViaVarejo.Properties.Resources._cH8iSbN_400x400;
            this.pictureBox2.Location = new System.Drawing.Point(1, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 27);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // Fechar
            // 
            this.Fechar.AutoSize = true;
            this.Fechar.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fechar.Location = new System.Drawing.Point(793, 2);
            this.Fechar.Name = "Fechar";
            this.Fechar.Size = new System.Drawing.Size(18, 18);
            this.Fechar.TabIndex = 2;
            this.Fechar.Text = "X";
            this.Fechar.Click += new System.EventHandler(this.Fechar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 15F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(32, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Gerenciador de Automação";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(36)))), ((int)(((byte)(21)))));
            this.panel2.Location = new System.Drawing.Point(-1, 295);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(812, 29);
            this.panel2.TabIndex = 1;
            // 
            // Operacao
            // 
            this.Operacao.AutoSize = true;
            this.Operacao.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Operacao.Location = new System.Drawing.Point(12, 96);
            this.Operacao.Name = "Operacao";
            this.Operacao.Size = new System.Drawing.Size(72, 20);
            this.Operacao.TabIndex = 2;
            this.Operacao.Text = "Operação:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ambiente:";
            // 
            // play
            // 
            this.play.Image = global::Robo_ViaVarejo.Properties.Resources._12443;
            this.play.Location = new System.Drawing.Point(488, 96);
            this.play.Name = "play";
            this.play.Size = new System.Drawing.Size(52, 50);
            this.play.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.play.TabIndex = 7;
            this.play.TabStop = false;
            this.play.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::Robo_ViaVarejo.Properties.Resources.ViaVarejo_Logo_VVar;
            this.pictureBox1.Location = new System.Drawing.Point(632, 210);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(170, 82);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // Operacao_Robo
            // 
            this.Operacao_Robo.FormattingEnabled = true;
            this.Operacao_Robo.Location = new System.Drawing.Point(90, 96);
            this.Operacao_Robo.Name = "Operacao_Robo";
            this.Operacao_Robo.Size = new System.Drawing.Size(327, 21);
            this.Operacao_Robo.TabIndex = 8;
            this.Operacao_Robo.SelectedIndexChanged += new System.EventHandler(this.listFluxo_Robo_SelectedIndexChanged);
            // 
            // listAmbiente
            // 
            this.listAmbiente.FormattingEnabled = true;
            this.listAmbiente.Location = new System.Drawing.Point(90, 48);
            this.listAmbiente.Name = "listAmbiente";
            this.listAmbiente.Size = new System.Drawing.Size(180, 21);
            this.listAmbiente.TabIndex = 9;
            this.listAmbiente.SelectedIndexChanged += new System.EventHandler(this.listAmbiente_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 278);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "V. 0.0.1";
            // 
            // SystemStatus
            // 
            this.SystemStatus.AutoSize = true;
            this.SystemStatus.Location = new System.Drawing.Point(87, 229);
            this.SystemStatus.Name = "SystemStatus";
            this.SystemStatus.Size = new System.Drawing.Size(0, 13);
            this.SystemStatus.TabIndex = 11;
            // 
            // Facility_Select
            // 
            this.Facility_Select.FormattingEnabled = true;
            this.Facility_Select.Location = new System.Drawing.Point(356, 47);
            this.Facility_Select.Name = "Facility_Select";
            this.Facility_Select.Size = new System.Drawing.Size(184, 21);
            this.Facility_Select.TabIndex = 12;
            this.Facility_Select.SelectedIndexChanged += new System.EventHandler(this.Facility_Select_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(300, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "Componente:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(561, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 20);
            this.label6.TabIndex = 14;
            this.label6.Text = "Filial:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // Filial_Select
            // 
            this.Filial_Select.FormattingEnabled = true;
            this.Filial_Select.Location = new System.Drawing.Point(603, 46);
            this.Filial_Select.Name = "Filial_Select";
            this.Filial_Select.Size = new System.Drawing.Size(184, 21);
            this.Filial_Select.TabIndex = 15;
            this.Filial_Select.SelectedIndexChanged += new System.EventHandler(this.Filial_Select_SelectedIndexChanged);
            // 
            // Gerenciador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(808, 308);
            this.Controls.Add(this.Filial_Select);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Facility_Select);
            this.Controls.Add(this.SystemStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listAmbiente);
            this.Controls.Add(this.Operacao_Robo);
            this.Controls.Add(this.play);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Operacao);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Gerenciador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gerenciador";
            this.Load += new System.EventHandler(this.Gerenciador_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.play)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Fechar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label Operacao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox play;
        private System.Windows.Forms.ComboBox Operacao_Robo;
        private System.Windows.Forms.ComboBox listAmbiente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label SystemStatus;
        private System.Windows.Forms.ComboBox Facility_Select;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Filial_Select;
    }
}