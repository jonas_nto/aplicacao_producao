﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Robo_ViaVarejo.Models.Aplicacao_TP
{
    class Functions : Excel_Process
    {
        [DllImport("user32.dll")]
        public static extern void SwitchToThisWindow(IntPtr hWnd, bool fAltTab);

        //Enumera os tipos para usar com o switch (o 1,2,3 são da API do user32)
        public enum Actions { Normal = 1, Minimize = 2, Maximize = 3 };

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);



        /// <summary>
        /// Fecha a aplicação pelo nome
        /// </summary>
        public static void Kill_Process(string Name_Process)
        {
            var process = Process.GetProcessesByName(Name_Process);
            foreach (var p in process)
                p.Kill();
        }

        public static bool MaximizaPCOM(Process processo)
        {
            try
            {
                ShowWindowAsync(processo.MainWindowHandle, (int)Actions.Maximize);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Erro ao maximizar a Tela do PCOM" + "Error: " + e.Message);
                return false;
            }
        }

        /// <summary>
        /// Preenche a tela preta do sistema
        /// </summary>
        /// <param name="tela">nome da tela</param>
        /// <param name="tecla">tecla do teclado</param>
        /// <param name="time">tempo estimado para aguardar antes de preencher</param>
        public static bool PreencheTelaPreta(string tela, string tecla, int time, Process processo)
        {
            try
            {
                //Alterna para tela preta
                SwitchToThisWindow(processo.MainWindowHandle, true);

                //envia tecla de comando101                
                SendKeys.SendWait(tecla);

                //Aguarda após preencher
                Thread.Sleep(time);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Erro ao enviar tecla: [" + tecla + "] na tela: [" + tela + "] com um aguardo de: [" + time + "]" + "Error: " + e.Message);
                return false;
            }
        }
    }
}