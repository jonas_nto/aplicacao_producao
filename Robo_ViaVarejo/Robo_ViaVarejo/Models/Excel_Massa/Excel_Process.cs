﻿using Robo_ViaVarejo.Controller;
using System;
using System.IO;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using Robo_ViaVarejo.Models.Aplicacao_TP;

public class Excel_Process : Driver
{
#pragma warning disable IDE0017

    //Variables for reading -> Workbook , Range and Excel Aplications
    public static Excel.Workbook xlWorkbook;
    public static Excel.Range XlRange;
    public static Excel.Application xlApp;
    public static Excel._Worksheet Worksheet;

    //Start to read Excel Cells
    public static int start_row;

    /// <summary>
    /// Abertura do Arquivo Gerenciador de Massa Excel [parametro: raiz da locaclizaçao do arquivo]
    /// </summary>
    public static void Open_Excel(string path)
    {
        //Define Excel Aplication
        xlApp = new Excel.Application();

        //Define the Excel View [ true - Show the Spreadsheet | False - hidden the Spreadsheet ] 
        xlApp.Visible = true;
        try
        {
            //Open Excel Document
            xlWorkbook = xlApp.Workbooks.Open(path);
        }
        catch (COMException e)
        {
            Console.WriteLine("Planilha excel.xlsx não foi encontrada, verifique! " + e.Message);
            Environment.Exit(0);
        }
    } //End Open_Excel()

    public class Result_OB
    {
        public string Valor { get; set; }
    }//End Class Result_OB

    public enum Option { Read, Record, Print }
    /// <summary>
    /// Leitura, Gravação e Print Excel 
    /// </summary>
    public static Result_OB Read_Record(Option option, string Sheet_Name, string Field_Name, string Record_Value, string Field_Print = "")
    {
        // If it's read-only "Read" in the option and in the parameter valor_gravar [ apply Record_Value = null]
        try
        {
            int Value_y;
            int row_sel;
            string Field_Value;
            start_row = Controlador.row_start;
            row_sel = start_row;

            //Define the Sheet Name
            Worksheet = xlWorkbook.Sheets[Sheet_Name];

            //Uses the range of selected sheet
            XlRange = Worksheet.UsedRange;

            //Return the location Value [y] according to the orientation [x,y], sending only the name of the column you want to find
            Value_y = Return_Sheet_Position(Field_Name, XlRange);

            if (Value_y == 0)
            {
                return new Result_OB() { Valor = null };
            }

            //Returns the value of the cell using the start line and the return value of the position[y] of the previous script line
            Field_Value = Convert.ToString(XlRange.Cells[row_sel, Value_y].Value);

            //Read case
            switch (option)
            {
                case Option.Read:
                    //Returns the value using O.O
                    return new Result_OB() { Valor = Field_Value };
                case Option.Record:
                    try
                    {
                        //Writes to the cell using the start line and the return value of the position[y] the value imputed in the Record_Value parameter
                        XlRange.Cells[row_sel, Value_y] = Record_Value;
                        return null;
                    }
                    catch (Exception e)
                    {
                        //("Problemas ao Gravar valor na célula")
                        Console.WriteLine(e.Message);
                        return null;
                    }
                default:
                    return null;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
    } //Fim Trata_Dados()

    public static int Return_Sheet_Position(string Column_Name, Excel.Range Planilha)
    {
        //Count the number of columns
        int QuatidadedeColunas = Planilha.Columns.Count;

        try
        {
            //value_Col starts at 1 to begin with A, but { (Spreadsheet.Cells[2, value_Col]}), the value 2 has to be changed to the start line of the column names
            for (int value_Col = 1; value_Col <= QuatidadedeColunas; value_Col++)
                if (Column_Name == Convert.ToString(Planilha.Cells[2, value_Col].Value).Trim())
                    return value_Col;//Returns the value[y] to use to find the orientation[x, y] 
        }
        catch (Exception e)
        {
            //Functions.Trata_Incidente("Problema ao Localizar a coluna : " + NomeColuna);
            Console.WriteLine(e.Message);

            return 0;
        }
        return 0;
    }//Fim Return_Sheet_Position()
} // End Class Excel_Process


//Uses Exemple
//**************************************************************************************************************
//Teste de strings
//string txt = Convert.ToString(Read_Record("Ler", "Executar", "Nome do teste", null).Valor);
//String[] array_txt = Multiple_Values(txt);
//foreach (var Linha_Valor in array_txt)
//{

//}

//string txt2 = Convert.ToString(Read_Record("Ler", "Executar", "Status", null).Valor);
//**************************************************************************************************************