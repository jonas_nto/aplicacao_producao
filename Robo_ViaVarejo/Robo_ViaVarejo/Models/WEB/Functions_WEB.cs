﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;

namespace Robo_ViaVarejo.Models.WEB
{
    class Functions_WEB : Excel_Process
    {
        /// <summary>
        /// Função Verifica a Existência de um Objeto na Tela, Parâmetros: By[Tipo {Id, ClassName, XPath...} ou Elemento IWebElement]
        /// </summary>
        public static bool VerificaObjetoExistente(By Type, bool Obrigatory = false, IWebElement elemento = null, int tentativas = 40)
        {
#pragma warning disable CS0162 // Código inacessível detectado
            for (int i = 0; i < tentativas; i++)
#pragma warning restore CS0162 // Código inacessível detectado
                try
                {
                    if (elemento != null)
                        if (elemento.Displayed && elemento.Enabled)
                            return true;

                    if (driver.FindElement(Type).Displayed && driver.FindElement(Type).Enabled)
                        return true;

                    if (Obrigatory == false)
                        return false;
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                }

            return false;
        }

        /// <summary>
        /// Altera para o frame solicitado no parametro
        /// </summary>
        public static bool MudaFrame(string nomeFrame, int tentativas = 20)
        {
            try
            {
                for (int i = 0; i <= tentativas; i++)
                    try
                    {
                        IWebElement iframe = Driver.driver.FindElement(By.Id(nomeFrame));
                        Driver.driver.SwitchTo().Frame(iframe);
                        return true;
                    }
                    catch
                    {
                        Thread.Sleep(1000);
                    }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Cria o elemento baseado no elemento primario e depois pelo tagname
        /// </summary>
        public static IWebElement BuscaTagElement(By byElement, string tagName, int tentativas = 20)
        {
            try
            {
                for (int i = 0; i < tentativas; i++)
                    try
                    {
                        IWebElement element = driver.FindElements(byElement)[0].FindElements(By.TagName(tagName))[0];
                        return element;
                    }
                    catch (Exception)
                    {
                    }

                return null;
            }
            catch (Exception)
            {
                return null;
            }

        }

        /// <summary>
        /// Preenche Campo Multifuncional (By[Tipo {Id, ClassName, XPath...}, valor a inputar no campo, quantidade de tentativas a preencher]
        /// </summary>
        public static bool PreencheValorCampoSetSelectButton(By Type, string valor, int tentativas = 40)
        {
            for (int i = 0; i < tentativas; i++)
                try
                {
                    if (VerificaObjetoExistente(Type, true, null, 1))
                    {
                        string htmltag = driver.FindElement(Type).TagName;
                        string type_element = driver.FindElement(Type).GetAttribute("type");
                        IWebElement elementjava = driver.FindElement(Type);
                        IJavaScriptExecutor javaclick = driver;

                        if (type_element != null && type_element.Equals("submit"))
                            htmltag = "SUBMIT";

                        if (type_element == null && htmltag.Equals("div"))
                            htmltag = "BUTTON";

                        if (htmltag.ToUpper().Trim().Equals("INPUT") && type_element.ToUpper().Trim().Equals("IMAGE"))
                            htmltag = "BUTTON";

                        if (htmltag.ToUpper().Trim().Equals("INPUT") && type_element.ToUpper().Trim().Equals("BUTTON"))
                            htmltag = "BUTTON";

                        if (htmltag.ToUpper().Trim().Equals("INPUT") && type_element.ToUpper().Trim().Equals("CHECKBOX"))
                            htmltag = "BUTTON";

                        if (htmltag.ToUpper().Trim().Equals("INPUT") && type_element.ToUpper().Trim().Equals("RADIO"))
                            htmltag = "BUTTON";

                        if (htmltag.ToUpper().Trim().Equals("H3"))
                            htmltag = "BUTTON";

                        if (htmltag.ToUpper().Trim().Equals("SPAN"))
                            htmltag = "BUTTON";

                        switch (htmltag.ToUpper().Trim())
                        {
                            case "INPUT":
                                for (int x = 0; x <= 100; x++)
                                {
                                    if (!driver.FindElement(Type).GetAttribute("value").Equals(""))
                                    {
                                        driver.FindElement(Type).SendKeys(Keys.ArrowRight);
                                        driver.FindElement(Type).SendKeys(Keys.Backspace);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                driver.FindElement(Type).SendKeys(valor);
                                return true;

                            case "SUBMIT":
                                javaclick.ExecuteScript("arguments[0].click();", elementjava);
                                return true;

                            case "TEXTAREA":

                                for (int x = 0; x <= 100; x++)
                                {
                                    if (!driver.FindElement(Type).GetAttribute("value").Equals(""))
                                    {
                                        driver.FindElement(Type).SendKeys(Keys.ArrowRight);
                                        driver.FindElement(Type).SendKeys(Keys.Backspace);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }

                                driver.FindElement(Type).SendKeys(valor);
                                return true;

                            case "SELECT":
                                var selectElement = new SelectElement(driver.FindElement(Type));
                                selectElement.SelectByText(valor);
                                return true;

                            case "BUTTON":
                                javaclick.ExecuteScript("arguments[0].click();", elementjava);
                                return true;

                            case "A":
                                javaclick.ExecuteScript("arguments[0].click();", elementjava);
                                return true;

                            default:
                                return false;
                        }
                    }
                }
                catch (Exception)
                {
                    Thread.Sleep(500);
                }
            return false;
        }

        //Kill Processes
        public static void Kill_Process(string Name_Process)
        {
            var process = Process.GetProcessesByName(Name_Process);
            foreach (var p in process)
                p.Kill();
        } // End Public Static Void Kill_Process

        public static String[] Multiple_Values(string Valor_Campo_Multiplo, Char PipeCort = '|')
        {
            String Valor_Campo = Valor_Campo_Multiplo;
            String[] Valores_Segmentado = Valor_Campo.Split(PipeCort);

            return Valores_Segmentado;
        }//End public static Multiple_Values



        public static String[] Multiple_Values_Sequence(string Valor_Campo_Multiplo)
        {
            String Valor_Campo = Valor_Campo_Multiplo;
            Char PipeCort = '>';
            String[] Valores_Segmentado = Valor_Campo.Split(PipeCort);

            return Valores_Segmentado;
        }//End public static Multiple_Values_Sequence


        /// <summary>
        /// Preenche input do Menu com texto desejado
        /// </summary>       
        public static bool Menu_Tipo(string xpath_btn, string input_search, string texto_input, string texto_completo_busca)
        {
            try
            {
                IWebElement input = null;
                string id_txt = "";

                //Muda para frame principal
                driver.SwitchTo().DefaultContent();

                //Botao de Menu
                if (Functions_WEB.PreencheValorCampoSetSelectButton(By.XPath(xpath_btn), null) == false)
                    return false;

                //Imput Box do search
                Thread.Sleep(2000);
                Functions_WEB.VerificaObjetoExistente(By.ClassName(input_search));

                for (int i = 0; i < 20; i++)
                    try
                    {
                        foreach (var item in driver.FindElements(By.TagName("input")))
                        {
                            if (item.Displayed == true && item.Enabled == true)
                            {
                                input = item;
                                break;
                            }
                        }
                        
                        Functions_WEB.VerificaObjetoExistente(null, true, input);
                        input.SendKeys(texto_input);
                        Thread.Sleep(1000);
                        break;
                    }
                    catch
                    {
                        Functions_WEB.PreencheValorCampoSetSelectButton(By.XPath("//*[@id='" + id_txt + "']"), null); //Clica Menu
                        Thread.Sleep(1000);
                    }

                //Verifica DropDown
                if (Functions_WEB.VerificaObjetoExistente(By.ClassName("x-boundlist-list-ct")) == false)
                    return false;

                string txt_list = driver.FindElement(By.ClassName("x-boundlist-list-ct")).GetAttribute("Id");
                IWebElement menulist = driver.FindElement(By.XPath("//*[@id='" + txt_list + "']"));
                IList menu_li = menulist.FindElements(By.TagName("li"));

                //Seleção do item no DropDown
                foreach (IWebElement li in menu_li)                
                    try
                    {
                        string text = li.GetAttribute("innerText");
                        if (text.Equals(texto_completo_busca))
                        {
                            IWebElement elementjava = li;
                            IJavaScriptExecutor javaclick = driver;
                            javaclick.ExecuteScript("arguments[0].click();", elementjava);
                            break;
                        }
                    }
                    catch { }
                
                return true;
            }
            catch (Exception) { return false; }
        }

        /// <summary>
        /// Muda para o frame escolhido, parametro numero do frame
        /// </summary>        
        public static bool MudaFrameTela(int numeroFrame)
        {
            try
            {
                //Muda para Frame Principal
                driver.SwitchTo().DefaultContent();

                //Muda para frame escolhido
                for (int i = 0; i < 20; i++)
                    try
                    {
                        if (driver.FindElements(By.TagName("iframe")).Count > 0)
                        {
                            driver.SwitchTo().Frame(numeroFrame - 1);
                            break;
                        }
                    }
                    catch (Exception)
                    {
                        Thread.Sleep(500);
                    }

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
        public static bool Fecha_menu(int janela)
        {
            try
            {
                driver.SwitchTo().DefaultContent();
                Thread.Sleep(500);
                string fecha = driver.FindElements(By.XPath("//img[contains(@class, 'x-tool-img') and contains(@class,'x-tool-close')]"))[janela].GetAttribute("id");
                driver.FindElement(By.Id(fecha)).Click();
                return true;
            }
            catch
            {
                string fecha = driver.FindElements(By.XPath("//img[contains(@class,'x-tool-close')]"))[janela + 1].GetAttribute("id");
                driver.FindElement(By.Id(fecha)).Click();
                return true;
            }
        }
    }
}